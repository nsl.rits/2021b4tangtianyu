/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#define NS_LOG_APPEND_CONTEXT                                   \
  if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; }

#include "tty-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include <algorithm>
#include <limits>
#include "ns3/constant-velocity-mobility-model.h"
#include <math.h>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <random>
#include <time.h>
#include <map>

#include "ns3/mobility-module.h"
#include "ns3/mobility-helper.h"
#include "ns3/vector.h"

#include <stdio.h>
#include <chrono>

#define INF 10000000
#define SIZE 100
#define TRUE 1
#define FALSE 0

int DIST[SIZE][SIZE];
int COST[SIZE];
int shi[16][4]={{1,10,0,10},{0,10,1,10},{1,10,0,10},{0,10,1,10},{1,10,0,10},{0,10,1,10},{1,10,0,10},{0,10,1,10},{0,10,1,10},{1,10,0,10},{0,10,1,10},{1,10,0,10},{1,10,0,10},{0,10,1,10},{1,10,0,10},{0,10,1,10}};
int VIA[SIZE];
int N = 16;
char USED[SIZE];
int ni=0;
int dijkstra(int start, int goal)
{
  int min, target;

  COST[start] = 0;

  while(1){

    /* 未確定の中から距離が最も小さい地点(a)を選んで、その距離を その地点の最小距離として確定します */
    min = INF;
    for(int i = 0; i < N; i++){
      if(!USED[i] && min > COST[i]) {
        min = COST[i];
        target = i;
      }
    }

    /* 全ての地点の最短経路が確定 */
    if(target == goal)
      return COST[goal];

    /* 今確定した場所から「直接つながっている」かつ「未確定の」地点に関して、
    今確定した場所を経由した場合の距離を計算し、今までの距離よりも小さければ書き直します。 */
    for(int neighboring = 0; neighboring < N; neighboring++){
      if(COST[neighboring] > DIST[target][neighboring] + COST[target]) {
        COST[neighboring] = DIST[target][neighboring] + COST[target];
        VIA[neighboring] = target;
      }
    }
    USED[target] = TRUE;
  }
}
int directionx[SIZE]={};
int directionx1[SIZE]={};
int directiony[SIZE]={};
int directiony1[SIZE]={};
int RSUroad[48]={}; //{}で全部ゼロで初期化される！
int ucd=0; //RSUroadの初期化用
int path[7]={};

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("ttyRoutingProtocol");

namespace tty {
NS_OBJECT_ENSURE_REGISTERED (ttyRoutingProtocol);

/// UDP Port for tty control traffic
const uint32_t ttyRoutingProtocol::tty_PORT = 654;


ttyRoutingProtocol::ttyRoutingProtocol ()
{
  
}

TypeId
ttyRoutingProtocol::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::tty::ttyRoutingProtocol")
    .SetParent<Ipv4RoutingProtocol> ()
    .SetGroupName ("tty")
    .AddConstructor<ttyRoutingProtocol> ()
    .AddAttribute ("UniformRv",
                   "Access to the underlying UniformRandomVariable",
                   StringValue ("ns3::UniformRandomVariable"),
                   MakePointerAccessor (&ttyRoutingProtocol::m_uniformRandomVariable),
                   MakePointerChecker<UniformRandomVariable> ())
  ;
  return tid;
}



ttyRoutingProtocol::~ttyRoutingProtocol ()
{
}

void
ttyRoutingProtocol::DoDispose ()
{

}

void
ttyRoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit) const
{
  *stream->GetStream () << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
                        << "; Time: " << Now ().As (unit)
                        << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (unit)
                        << ", tty Routing table" << std::endl;

  //Print routing table here.
  *stream->GetStream () << std::endl;
}

int64_t
ttyRoutingProtocol::AssignStreams (int64_t stream)
{
  NS_LOG_FUNCTION (this << stream);
  m_uniformRandomVariable->SetStream (stream);
  return 1;
}



Ptr<Ipv4Route>
ttyRoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header,
                              Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
{

  std::cout<<"Route Ouput Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
  Ptr<Ipv4Route> route;

  if (!p)
    {
	  std::cout << "loopback occured! in routeoutput";
	  return route;// LoopbackRoute (header,oif);

	}

  if (m_socketAddresses.empty ())
    {
	  sockerr = Socket::ERROR_NOROUTETOHOST;
	  NS_LOG_LOGIC ("No zeal interfaces");
	  std::cout << "RouteOutput No zeal interfaces!!, packet drop\n";

	  Ptr<Ipv4Route> route;
	  return route;
    }





  
  return route;
}



bool
ttyRoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                             Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                             MulticastForwardCallback mcb, LocalDeliverCallback lcb, ErrorCallback ecb)
{
 
  std::cout<<"Route Input Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
  return true;
}



void
ttyRoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
{
  NS_ASSERT (ipv4 != 0);
  NS_ASSERT (m_ipv4 == 0);
  m_ipv4 = ipv4;
}

void
ttyRoutingProtocol::NotifyInterfaceUp (uint32_t i)
{
  NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ()
                        << " interface is up");
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
  if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
    {
      return;
    }
  // Create a socket to listen only on this interface
  Ptr<Socket> socket;

  socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&ttyRoutingProtocol::Recvtty,this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (iface.GetLocal (), tty_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetIpRecvTtl (true);
  m_socketAddresses.insert (std::make_pair (socket,iface));


    // create also a subnet broadcast socket
  socket = Socket::CreateSocket (GetObject<Node> (),
                                 UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&ttyRoutingProtocol::Recvtty, this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (iface.GetBroadcast (), tty_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetIpRecvTtl (true);
  m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));


  if (m_mainAddress == Ipv4Address ())
    {
      m_mainAddress = iface.GetLocal ();
    }

  NS_ASSERT (m_mainAddress != Ipv4Address ());


/*  for (uint32_t i = 0; i < m_ipv4->GetNInterfaces (); i++)
        {

          // Use primary address, if multiple
          Ipv4Address addr = m_ipv4->GetAddress (i, 0).GetLocal ();
        //  std::cout<<"############### "<<addr<<" |ninterface "<<m_ipv4->GetNInterfaces ()<<"\n";
       

              TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
              Ptr<Node> theNode = GetObject<Node> ();
              Ptr<Socket> socket = Socket::CreateSocket (theNode,tid);
              InetSocketAddress inetAddr (m_ipv4->GetAddress (i, 0).GetLocal (), tty_PORT);
              if (socket->Bind (inetAddr))
                {
                  NS_FATAL_ERROR ("Failed to bind() ZEAL socket");
                }
              socket->BindToNetDevice (m_ipv4->GetNetDevice (i));
              socket->SetAllowBroadcast (true);
              socket->SetRecvCallback (MakeCallback (&RoutingProtocol::Recvtty, this));
              //socket->SetAttribute ("IpTtl",UintegerValue (1));
              socket->SetRecvPktInfo (true);

              m_socketAddresses[socket] = m_ipv4->GetAddress (i, 0);

              //  NS_LOG_DEBUG ("Socket Binding on ip " << m_mainAddress << " interface " << i);

              break;
           
        }
*/

}

void
ttyRoutingProtocol::NotifyInterfaceDown (uint32_t i)
{
  
}

void
ttyRoutingProtocol::NotifyAddAddress (uint32_t i, Ipv4InterfaceAddress address)
{
 
}

void
ttyRoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
{
 
}



void
ttyRoutingProtocol::DoInitialize (void)
{

  int8_t id =m_ipv4->GetObject<Node> ()->GetId ();
  if(id >= 16)   //if Node ID = 0
    {
      std::cout<<"hello will be send\n";
      SendHello();
      //Simulator::Schedule(Seconds(2), &ttyRoutingProtocol::SendXBroadcast, this);
    } 
   if(id == 16)   //if Node ID = 0
    {
      std::cout<<"broadcast will be send\n";
      Simulator::Schedule(Seconds(5), &ttyRoutingProtocol::SendBroadcast, this);
      //Simulator::Schedule(Seconds(2), &ttyRoutingProtocol::SendXBroadcast, this);
    } 

}



void
ttyRoutingProtocol::SendXBroadcast (void)
{
 for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
       != m_socketAddresses.end (); ++j)
    {

      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<Packet> packet = Create<Packet> ();

      RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3));
      packet->AddHeader (rrepHeader);
      
      TypeHeader tHeader (ttyTYPE_RREP);
      packet->AddHeader (tHeader);
       
      // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
      Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        {
          destination = iface.GetBroadcast ();
        }
      socket->SendTo (packet, 0, InetSocketAddress (destination, tty_PORT));
      std::cout<<"broadcast sent\n"; 
        
     }
}


void
ttyRoutingProtocol::Recvtty (Ptr<Socket> socket)
{
  //printf("Recvtty\n");
  Address sourceAddress;
  Ptr<Packet> packet = socket->RecvFrom (sourceAddress);
  InetSocketAddress inetSourceAddr = InetSocketAddress::ConvertFrom (sourceAddress);
  Ipv4Address sender = inetSourceAddr.GetIpv4 ();
  Ipv4Address receiver;

  if (m_socketAddresses.find (socket) != m_socketAddresses.end ())
    receiver = m_socketAddresses[socket].GetLocal ();
    

  TypeHeader tHeader (HELLOTYPE);
  packet->RemoveHeader (tHeader);

  switch(tHeader.Get()){
    case HELLOTYPE:
      RecvHello(packet,receiver,sender);
      break;
    case BROADCASTTYPE:
      RecvBroadcast(packet,receiver,sender);
      break;
    default:
      std::cout << "Unknown Type received in " << receiver << " from " << sender << "\n";
      break;

  }
}

void 
ttyRoutingProtocol::SendTo(Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination)
{
  //printf("SendTo\n");
  socket->SendTo (packet, 0, InetSocketAddress (destination, tty_PORT));
}

void
ttyRoutingProtocol::SendHello()
{
  //printf("SendHello\n");
  //std::cout<<"In sendhello(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
    {
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
      Vector pos = mobility->GetPosition ();
      //Ptr<ConstantVelocityMobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<ConstantVelocityMobilityModel>();
      //Vector velocity = mobility->GetVelocity ();
      HelloHeader helloHeader;
      Ptr<Packet> packet = Create<Packet> ();
      helloHeader.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
      helloHeader.SetPosition_X(pos.x);
      helloHeader.SetPosition_Y(pos.y);
      //helloHeader.Setspeedx(velocity.x);
      //helloHeader.Setspeedy(velocity.y);
      packet->AddHeader(helloHeader);
      TypeHeader tHeader (HELLOTYPE);
      packet->AddHeader (tHeader);
      directionx[m_ipv4->GetObject<Node> ()->GetId ()]=pos.x;
      //printf("xxxxxxxxxxxxxx=%d\n",directionx[m_ipv4->GetObject<Node> ()->GetId ()]);
      directiony[m_ipv4->GetObject<Node> ()->GetId ()]=pos.y;
      //printf("yyyyyyyyyyyyyy=%d\n",directiony[m_ipv4->GetObject<Node> ()->GetId ()]);
      ucd=0;
      Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
          std::cout<<"broadcast sent\n";
        }
      else
        { 
          destination = iface.GetBroadcast ();
        }
      Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0,150)));
      Simulator::Schedule (jitter, &ttyRoutingProtocol::SendTo, this , socket, packet, destination);
      Simulator::Schedule (Seconds(2), &ttyRoutingProtocol::SendHello, this );
      

    }
    if(m_ipv4->GetObject<Node>()->GetId()==17){
    for(int i=0;i<16;i++){
     printf("%d   %d\n",shi[i][0],shi[i][2]);
     shi[i][1]=shi[i][1]-2;
     shi[i][3]=shi[i][3]-2;
     if(shi[i][1]<=0 && shi[i][0]==1){
      shi[i][1]=10;
      shi[i][0]=0;
     }
     if(shi[i][1]<=0 && shi[i][0]==0){
      shi[i][1]=10;
      shi[i][0]=1;
     }
     if(shi[i][3]<=0 && shi[i][2]==1){
      shi[i][3]=10;
      shi[i][2]=0;
     }
     if(shi[i][3]<=0 && shi[i][2]==0){
      shi[i][3]=10;
      shi[i][2]=1;
     }
    //printf("%d   %d\n",shi[i][0],shi[i][2]);
    }
    }
    //printf("aaaaaaaaaaaaaaaaaaaaaaaaaaaaa\n");
    if(m_ipv4->GetObject<Node>()->GetId()>=16){
    Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
    Vector pos = mobility->GetPosition ();
    if(pos.y==0 && pos.x>400 && pos.x<430){
     mainmove1();
    }
    //if(pos.y==0 && pos.x>800 && pos.x<830){
     //mainmove2();
    //}
    //if(pos.y==0 && pos.x>1180 && pos.x<1210){
     //mainmove3();
    //}
    //if(pos.x==0 && pos.y<830 && pos.y>770){
     //mainmove4();
    //}
    //if(pos.x==0 && pos.y<430 && pos.y>370){
     //mainmove5();
    //}
    } 
    if(m_ipv4->GetObject<Node>()->GetId()>=16){
    Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
    Vector pos = mobility->GetPosition ();
    if(pos.x<0){
     rightmove();
    }else if(pos.y<0){
     mainmove();
    }else if(pos.y>1200){
     ymainmove();
    }else if(pos.x>1200){
     leftmove();
    }
    }    
}

void
ttyRoutingProtocol::RecvHello(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
{
  //printf("RecvHello\n");
  //std::cout<<"In recvhello(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  //ni=ni+1;
  //printf("%d\n",ni);
  HelloHeader helloHeader;
  packet->RemoveHeader(helloHeader);
  uint8_t nodeId = helloHeader.GetNodeId();
  uint16_t pos_x = helloHeader.GetPosition_X();
  uint16_t pos_y = helloHeader.GetPosition_Y();
  //uint16_t speed = helloHeader.GetPosition_speed();
  //printf("nodeId=%d\n",nodeId);
  uint16_t rid = m_ipv4->GetObject<Node> ()->GetId ();
  //printf("nodeId=%d\n",rid);
  uint16_t dr = directionx[nodeId] + directiony[nodeId];
  //printf("aaaaaaaaaaaaaaa=%d\n",dr);
  int xr=0;
  int xl=0;
  int yr=0;
  int yl=0;
  //printf("xxx=%d\n",directionx[nodeId]);
  //printf("yyy=%d\n",directiony[nodeId]);
  if(directionx[nodeId]<directionx1[nodeId]){
   xr=1;
  }else if(directiony[nodeId]>directiony1[nodeId]){
   yr=1;
  }else if(directionx[nodeId]>directionx1[nodeId]){
   xl=1;
  }else if(directiony[nodeId]<directiony1[nodeId]){
   yl=1;
  }
  if(rid>16){
    return;
  }
  //printf("eeeee=%d\n",xr);
  //printf("qqqqq=%d\n",xl);
  //printf("wwwww=%d\n",yr);
  //printf("rrrrr=%d\n",yl);
  if(ucd==0){
    for(int i=0;i<=47;i++)
    {
      RSUroad[i]=0;
    }
    ucd++;
  }

  if(m_ipv4->GetObject<Node> ()->GetId () == 0){      
      if(0<=pos_x && pos_x<400 && pos_y==0 && dr==0){
        RSUroad[0]+=1;
      }else if(pos_x==0 && 0<=pos_y && pos_y<400 && dr==0){
        RSUroad[24]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==0 && dr>0 && xr==1){
        RSUroad[0]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==0 && dr>0 && xl==1){
        RSUroad[1]+=1;
      }else if(pos_x==0 && 0<=pos_y && pos_y<400 && dr>0 && yr==1 ){
        RSUroad[24]+=1;
      }else if(pos_x==0 && 0<=pos_y && pos_y<400 && dr>0 && yl==1){
        RSUroad[25]+=1;
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 1){
      if(0<=pos_x && pos_x<400 && pos_y==0 && dr==0){
        RSUroad[0]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==0 && dr==0){
        RSUroad[2]+=1;
      }else if(pos_x==400 && 0<=pos_y && pos_y<400 && dr==0){
        RSUroad[26]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==0 && dr>0 && xr==1){
        RSUroad[0]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==0 && dr>0 && xl==1){
        RSUroad[1]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==0 && dr>0 && xr==1){
        RSUroad[2]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==0 && dr>0 && xl==1){
        RSUroad[3]+=1;
      }else if(pos_x==400 && 0<=pos_y && pos_y<400 && dr>0 && yr==1){
        RSUroad[26]+=1;
      }else if(pos_x==400 && 0<=pos_y && pos_y<400 && dr>0 && yr==1){
        RSUroad[27]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 2){
      if(400<=pos_x && pos_x<800 && pos_y==0 && dr==0){
        RSUroad[2]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==0 && dr==0){
        RSUroad[4]+=1;
      }else if(pos_x==800 && 0<=pos_y && pos_y<400 && dr==0){
        RSUroad[28]+=1;   
      }else if(400<=pos_x && pos_x<800 && pos_y==0 && dr>0 && xr==1){
        RSUroad[2]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==0 && dr>0 && xl==1){
        RSUroad[3]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==0 && dr>0 && xr==1){
        RSUroad[4]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==0 && dr>0 && xl==1){
        RSUroad[5]+=1;
      }else if(pos_x==800 && 0<=pos_y && pos_y<400 && dr>0 && yr==1){
        RSUroad[28]+=1;   
      }else if(pos_x==800 && 0<=pos_y && pos_y<400 && dr>0 && yl==1){
        RSUroad[29]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 3){      
      if(800<=pos_x && pos_x<1200 && pos_y==0 && dr==0){
        RSUroad[4]+=1;
      }else if(pos_x==1200 && 0<=pos_y && pos_y<400 && dr==0){
        RSUroad[30]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==0 && dr>0 && xr==1){
        RSUroad[4]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==0 && dr>0 && xl==1){
        RSUroad[5]+=1;
      }else if(pos_x==1200 && 0<=pos_y && pos_y<400 && dr>0 && yr==1){
        RSUroad[30]+=1;
      }else if(pos_x==1200 && 0<=pos_y && pos_y<400 && dr>0 && yl==1){
        RSUroad[31]+=1;
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 4){
      if(0<=pos_x && pos_x<400 && pos_y==400 && dr==0){
        RSUroad[6]+=1;
      }else if(pos_x==0 && 0<=pos_y && pos_y<400 && dr==0){
        RSUroad[24]+=1;
      }else if(pos_x==0 && 400<=pos_y && pos_y<800 && dr==0){
        RSUroad[32]+=1;
      }else if(pos_x==0 && 0<=pos_y && pos_y<400 && dr>0 && yr==1){
        RSUroad[24]+=1;
      }else if(pos_x==0 && 0<=pos_y && pos_y<400 && dr>0 && yl==1){
        RSUroad[25]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==400 && dr>0 && xr==1){
        RSUroad[6]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==400 && dr>0 && xr==1){
        RSUroad[7]+=1;
      }else if(pos_x==0 && 400<=pos_y && pos_y<800 && dr>0 && yr==1){
        RSUroad[32]+=1;   
      }else if(pos_x==0 && 400<=pos_y && pos_y<800 && dr>0 && yl==1){
        RSUroad[33]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 5){
      if(0<=pos_x && pos_x<400 && pos_y==400 && dr==0){
        RSUroad[6]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==400 && dr==0){
        RSUroad[8]+=1;
      }else if(pos_x==400 && 0<=pos_y && pos_y<400 && dr==0){
        RSUroad[26]+=1;
      }else if(pos_x==400 && 400<=pos_y && pos_y<800 && dr==0){
        RSUroad[34]+=1;   
      }else if(0<=pos_x && pos_x<400 && pos_y==400 && dr>0 && xr==1){
        RSUroad[6]+=1;
      }else if(pos_x==400 && 0<=pos_y && pos_y<400 && dr>0 && xl==1){
        RSUroad[7]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==400 && dr>0 && xr==1){
        RSUroad[8]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==400 && dr>0 && xl==1){
        RSUroad[9]+=1;
      }else if(pos_x==400 && 0<=pos_y && pos_y<400 && dr>0 && yr==1){
        RSUroad[26]+=1;
      }else if(pos_x==400 && 0<=pos_y && pos_y<400 && dr>0 && yl==1){
        RSUroad[27]+=1;
      }else if(pos_x==400 && 400<=pos_y && pos_y<800 && dr>0 && yr==1){
        RSUroad[34]+=1;   
      }else if(pos_x==400 && 400<=pos_y && pos_y<800 && dr>0 && yl==1){
        RSUroad[35]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 6){
      if(400<=pos_x && pos_x<800 && pos_y==400 && dr==0){
        RSUroad[8]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==400 && dr>0 && xr==1){
        RSUroad[8]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==400 && dr>0 && xl==1){
        RSUroad[9]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==400 && dr==0){
        RSUroad[10]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==400 && dr>0 && xr==1){
        RSUroad[10]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==400 && dr>0 && xl==1){
        RSUroad[11]+=1;
      }else if(pos_x==800 && 0<=pos_y && pos_y<400 && dr==0){
        RSUroad[28]+=1;
      }else if(pos_x==800 && 0<=pos_y && pos_y<400 && dr>0 && yr==1){
        RSUroad[28]+=1;
      }else if(pos_x==800 && 0<=pos_y && pos_y<400 && dr>0 && yl==1){
        RSUroad[29]+=1;
      }else if(pos_x==800 && 400<=pos_y && pos_y<800 && dr==0){
        RSUroad[36]+=1;   
      }else if(pos_x==800 && 400<=pos_y && pos_y<800 && dr>0 && yr==1){
        RSUroad[36]+=1;   
      }else if(pos_x==800 && 400<=pos_y && pos_y<800 && dr>0 && yl==1){
        RSUroad[37]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 7){
      if(800<=pos_x && pos_x<1200 && pos_y==400 && dr==0){
        RSUroad[10]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==400 && dr>0 && xr==1){
        RSUroad[10]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==400 && dr>0 && xl==1){
        RSUroad[11]+=1;
      }else if(pos_x==1200 && 0<=pos_y && pos_y<400 && dr==0){
        RSUroad[30]+=1;
      }else if(pos_x==1200 && 0<=pos_y && pos_y<400 && dr>0 && yr==1){
        RSUroad[30]+=1;
      }else if(pos_x==1200 && 0<=pos_y && pos_y<400 && dr>0 && yl==1){
        RSUroad[31]+=1;
      }else if(pos_x==1200 && 400<=pos_y && pos_y<800 && dr==0){
        RSUroad[38]+=1;   
      }else if(pos_x==1200 && 400<=pos_y && pos_y<800 && dr>0 && yr==1){
        RSUroad[38]+=1;   
      }else if(pos_x==1200 && 400<=pos_y && pos_y<800 && dr>0 && yl==1){
        RSUroad[39]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 8){
      if(0<=pos_x && pos_x<400 && pos_y==800 && dr==0){
        RSUroad[12]+=1;
      }else if(pos_x==0 && 400<=pos_y && pos_y<800 && dr>0 && xr==1){
        RSUroad[12]+=1;
      }else if(pos_x==0 && 400<=pos_y && pos_y<800 && dr>0 && xl==1){
        RSUroad[13]+=1;
      }else if(pos_x==0 && 400<=pos_y && pos_y<800 && dr==0){
        RSUroad[32]+=1;
      }else if(pos_x==0 && 400<=pos_y && pos_y<800 && dr>0 && yr==1){
        RSUroad[32]+=1;
      }else if(pos_x==0 && 400<=pos_y && pos_y<800 && dr>0 && yl==1){
        RSUroad[33]+=1;
      }else if(pos_x==0 && 800<=pos_y && pos_y<1200 && dr==0){
        RSUroad[20]+=1;   
      }else if(pos_x==0 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[40]+=1;   
      }else if(pos_x==0 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[41]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 9){
      if(0<=pos_x && pos_x<400 && pos_y==800 && dr==0){
        RSUroad[12]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==800 && dr>0 && xr==1){
        RSUroad[12]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==800 && dr>0 && xl==1){
        RSUroad[13]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==800 && dr==0){
        RSUroad[14]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==800 && dr>0 && xr==1){
        RSUroad[14]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==800 && dr>0 && xl==1){
        RSUroad[15]+=1;
      }else if(pos_x==400 && 400<=pos_y && pos_y<800 && dr==0){
        RSUroad[34]+=1;
      }else if(pos_x==400 && 400<=pos_y && pos_y<800 && dr>0 && yr==1){
        RSUroad[34]+=1;
      }else if(pos_x==400 && 400<=pos_y && pos_y<800 && dr>0 && yl==1){
        RSUroad[35]+=1;
      }else if(pos_x==400 && 800<=pos_y && pos_y<1200 && dr==0){
        RSUroad[42]+=1;   
      }else if(pos_x==400 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[42]+=1;   
      }else if(pos_x==400 && 800<=pos_y && pos_y<1200 && dr>0 && yl==1){
        RSUroad[43]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 10){
      if(400<=pos_x && pos_x<800 && pos_y==800 && dr==0){
        RSUroad[14]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==800 && dr>0 && xr==1){
        RSUroad[14]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==800 && dr>0 && xl==1){
        RSUroad[15]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==800 && dr==0){
        RSUroad[16]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==800 && dr>0 && xr==1){
        RSUroad[16]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==800 && dr>0 && xl==1){
        RSUroad[17]+=1;
      }else if(pos_x==800 && 400<=pos_y && pos_y<800 && dr==0){
        RSUroad[36]+=1;
      }else if(pos_x==800 && 400<=pos_y && pos_y<800 && dr>0 && yr==1){
        RSUroad[36]+=1;
      }else if(pos_x==800 && 400<=pos_y && pos_y<800 && dr>0 && yl==1){
        RSUroad[37]+=1;
      }else if(pos_x==800 && 800<=pos_y && pos_y<1200 && dr==0){
        RSUroad[44]+=1;   
      }else if(pos_x==800 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[44]+=1;   
      }else if(pos_x==800 && 800<=pos_y && pos_y<1200 && dr>0 && yl==1){
        RSUroad[45]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 11){
      if(800<=pos_x && pos_x<1200 && pos_y==800 && dr==0){
        RSUroad[16]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==800 && dr>0 && xr==1){
        RSUroad[16]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==800 && dr>0 && xl==1){
        RSUroad[17]+=1;
      }else if(pos_x==1200 && 400<=pos_y && pos_y<800 && dr==0){
        RSUroad[38]+=1;
      }else if(pos_x==1200 && 400<=pos_y && pos_y<800 && dr>0 && yr==1){
        RSUroad[39]+=1;
      }else if(pos_x==1200 && 400<=pos_y && pos_y<800 && dr>0 && yl==1){
        RSUroad[19]+=1;
      }else if(pos_x==1200 && 800<=pos_y && pos_y<1200 && dr==0){
        RSUroad[46]+=1;   
      }else if(pos_x==1200 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[46]+=1;   
      }else if(pos_x==1200 && 800<=pos_y && pos_y<1200 && dr>0 && yl==1){
        RSUroad[47]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 12){
      if(0<=pos_x && pos_x<400 && pos_y==1200 && dr==0){
        RSUroad[18]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==1200 && dr>0 && xr==1){
        RSUroad[18]+=1;   
      }else if(0<=pos_x && pos_x<400 && pos_y==1200 && dr>0 && xl==1){
        RSUroad[19]+=1;   
      }else if(pos_x==0 && 800<=pos_y && pos_y<1200 && dr==0){
        RSUroad[40]+=1;   
      }else if(pos_x==0 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[40]+=1;   
      }else if(pos_x==0 && 800<=pos_y && pos_y<1200 && dr>0 && yl==1){
        RSUroad[41]+=1;   
      }
    }else if(m_ipv4->GetObject<Node> ()->GetId () == 13){
      if(0<=pos_x && pos_x<400 && pos_y==1200 && dr==0){
        RSUroad[18]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==1200 && dr>0 && xr==1){
        RSUroad[18]+=1;
      }else if(0<=pos_x && pos_x<400 && pos_y==1200 && dr>0 && xl==1){
        RSUroad[19]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==1200 && dr==0){
        RSUroad[20]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==1200 && dr>0 && xr==1){
        RSUroad[20]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==1200 && dr>0 && xl==1){
        RSUroad[21]+=1;
      }else if(pos_x==400 && 800<=pos_y && pos_y<1200 && dr==0){
        RSUroad[42]+=1;   
      }else if(pos_x==400 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[42]+=1;   
      }else if(pos_x==400 && 800<=pos_y && pos_y<1200 && dr>0 && yl==1){
        RSUroad[43]+=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 14){
      if(400<=pos_x && pos_x<800 && pos_y==1200 && dr==0){
        RSUroad[20]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==1200 && dr>0 && xr==1){
        RSUroad[20]+=1;
      }else if(400<=pos_x && pos_x<800 && pos_y==1200 && dr>0 && xl==1){
        RSUroad[21]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==1200 && dr==0){
        RSUroad[22]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==1200 && dr>0 && xr==1){
        RSUroad[22]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==1200 && dr>0 && xl==1){
        RSUroad[23]+=1;
      }else if(pos_x==800 && 800<=pos_y && pos_y<1200 && dr==0){
        RSUroad[44]-=1;   
      }else if(pos_x==800 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[44]-=1;   
      }else if(pos_x==800 && 800<=pos_y && pos_y<1200 && dr>0 && yl==1){
        RSUroad[45]-=1;   
      }
  }else if(m_ipv4->GetObject<Node> ()->GetId () == 15){      
      if(800<=pos_x && pos_x<1200 && pos_y==1200 && dr==0){
        RSUroad[22]+=1;
      }else if(800<=pos_x && pos_x<1200 && pos_y==1200 && dr>0 && xr==1){
        RSUroad[22]+=1;
      } else if(800<=pos_x && pos_x<1200 && pos_y==1200 && dr>0 && xl==1){
        RSUroad[23]+=1;
      } else if(pos_x==1200 && 800<=pos_y && pos_y<1200 && dr==0){
        RSUroad[46]+=1;
      } else if(pos_x==1200 && 800<=pos_y && pos_y<1200 && dr>0 && yr==1){
        RSUroad[46]+=1;
      } else if(pos_x==1200 && 800<=pos_y && pos_y<1200 && dr>0 && yl==1){
        RSUroad[47]+=1;
      } 
  }
  directionx1[nodeId]=directionx[nodeId];
  //printf("x111111111111=%d\n",directionx1[nodeId]);
  directiony1[nodeId]=directiony[nodeId];
  //printf("y1111111111111=%d\n",directiony1[nodeId]);
      /*for(int j=0;j<=47;j++)
    {
      printf("road%d:%d\n",j,RSUroad[j]);
    }*/
          
  //Time jitter = Time (Seconds (m_uniformRandomVariable->GetInteger (1, 2)));
  //Simulator::Schedule (jitter, &ttyRoutingProtocol::SendHello, this);
  
}

void
ttyRoutingProtocol::mainmove1(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    mobility->SetPosition(Vector(400,0,0));
    mobility->SetVelocity(Vector(0,12,0));
}

void
ttyRoutingProtocol::mainmove2(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    mobility->SetPosition(Vector(800,0,0));
    mobility->SetVelocity(Vector(0,12,0));
}

void
ttyRoutingProtocol::mainmove3(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    mobility->SetPosition(Vector(1200,0,0));
    mobility->SetVelocity(Vector(0,12,0));
}

void
ttyRoutingProtocol::mainmove4(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    mobility->SetPosition(Vector(0,800,0));
    mobility->SetVelocity(Vector(12,0,0));
}

void
ttyRoutingProtocol::mainmove5(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    mobility->SetPosition(Vector(0,400,0));
    mobility->SetVelocity(Vector(12,0,0));
}

void
ttyRoutingProtocol::mainmove(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    //mobility->SetPosition(Vector(100,0,0));
    mobility->SetVelocity(Vector(0,12,0));
}

void
ttyRoutingProtocol::rightmove(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    //mobility->SetPosition(Vector(100,0,0));
    mobility->SetVelocity(Vector(12,0,0));
}

void
ttyRoutingProtocol::leftmove(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    //mobility->SetPosition(Vector(100,0,0));
    mobility->SetVelocity(Vector(-12,0,0));
}

void
ttyRoutingProtocol::ymainmove(){
    //printf("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb\n");
    Ptr<ConstantVelocityMobilityModel> mobility =m_ipv4->GetObject<Node>()->GetObject<ConstantVelocityMobilityModel>();
    //mobility->SetPosition(Vector(100,0,0));
    mobility->SetVelocity(Vector(0,-12,0));
}

void
ttyRoutingProtocol::SendBroadcast()
{
   printf("SendBroadcast\n");
     
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
    {
     
    std::cout<<"In sendbroadcast(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
      Vector pos = mobility->GetPosition ();
      BROADCASTHeader broadcastHeader;
      Ptr<Packet> packet = Create<Packet> ();
      broadcastHeader.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
      broadcastHeader.SetPosition_X(pos.x);
      broadcastHeader.SetPosition_Y(pos.y);
      packet->AddHeader(broadcastHeader);

      TypeHeader tHeader (BROADCASTTYPE);
      packet->AddHeader (tHeader);

    Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        { 
          destination = iface.GetBroadcast ();
        }

      //std::cout<<destination<<"\n";

      socket->SendTo (packet, 0, InetSocketAddress (destination, tty_PORT));

      Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0,10)));
      Simulator::Schedule (jitter, &ttyRoutingProtocol::SendTo, this , socket, packet, destination);
      Simulator::Schedule (Seconds(5), &ttyRoutingProtocol::SendBroadcast, this );
    }
}

void
ttyRoutingProtocol::RecvBroadcast(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
{
  std::cout<<"abc sendbroadcast(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  BROADCASTHeader broadcastHeader;
  packet->RemoveHeader(broadcastHeader);
  uint16_t id = broadcastHeader.GetNodeId();
  //printf("%d\n",id);
  uint16_t rid = m_ipv4->GetObject<Node> ()->GetId ();
  //printf("%d\n",rid);

  if(id>16){
    return;
  }
  else if(rid<16){
  if(id==16){
  //int r=24; 
  //int a,b,l;
  int s=rid;
  int d=15;
  /* 初期化 */
  for(int i = 0; i < SIZE; i++) {
    COST[i] = INF;
    USED[i] = FALSE;
    VIA[i] = -1;
    for(int j = 0; j < SIZE; j++)
      DIST[i][j] = INF;
  }
  /* ロードDを格納していく */
    DIST[0][1] = RSUroad[0];
    DIST[0][4] = RSUroad[24];
    
    DIST[1][2] = RSUroad[2];
    DIST[1][5] = RSUroad[26];

    DIST[2][3] = RSUroad[4];
    DIST[2][6] = RSUroad[28];

    DIST[3][7] = RSUroad[30];

    DIST[4][5] = RSUroad[6];
    DIST[4][8] = RSUroad[32];

    DIST[5][6] = RSUroad[8];
    DIST[5][9] = RSUroad[34];

    DIST[6][7] = RSUroad[10];
    DIST[6][10] = RSUroad[36];

    DIST[7][11] = RSUroad[38];

    DIST[8][9] = RSUroad[12];
    DIST[8][12] = RSUroad[40];

    DIST[9][10] = RSUroad[14];
    DIST[9][13] = RSUroad[42];

    DIST[10][11] = RSUroad[16];
    DIST[10][14] = RSUroad[44];

    DIST[11][15] = RSUroad[46];


    DIST[12][13] = RSUroad[18];
    DIST[13][14] = RSUroad[20];
    DIST[14][15] = RSUroad[22];


  /* ダイクストラ法で最短経路を求める */
  //printf("distance:%d\n", dijkstra(s,d));
  dijkstra(s,d);
  /* 経路を表示 */
  
  int node = d; 
  //printf("%d\n", node);
  
  //VIA[node]　多分次のノードのこと
  
  path[6]= node;

  //ゴールから順になっているのでそれを逆にして正順にしている。
  for(int i=5;i>=0;--i){
    node=VIA[node];
    path[i]=node;
    //printf("path[%d]は%d\n",i,path[i]);
  }
  
  //確認
  printf("*****PATH*****\n");
    for(int i=0;i<=6;i++){
    path[i]+=1;
    printf("%d\n",path[i]);
  }
  printf("***************\n");
   /*for(int i=1;i<=16;i++){
   if(path[rid]==i){
    if(shi[i-1][0]==1){
    printf("path[%d]",i);
    std::cout<<"red time "<<shi[i][1]<<"\n";
   }
  }
  }*/
 }
}

}


} //namespace tty
} //namespace ns3
