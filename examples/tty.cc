#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-interface-container.h"
#include "ns3/v4ping-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/tty-module.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <time.h> 

NS_LOG_COMPONENT_DEFINE ("ttyProtocolMinimum");
using namespace ns3;


Ptr<ConstantVelocityMobilityModel> cvmm;

int 
main (int argc, char *argv[])
{
  //bool verbose = true;
  uint32_t nCsma = 16;
  uint32_t nWifi = 50;
  //uint32_t range =200;
  double heightField = 1600;
  double widthField = 1600;
  uint32_t step  =400;
  //bool tracing = false;

  //CommandLine cmd;
  //cmd.AddValue ("nCsma", "Number of \"extra\" CSMA nodes/devices", nCsma);
  //cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);
  //cmd.AddValue ("verbose", "Tell echo applications to log if true", verbose);
  //cmd.AddValue ("tracing", "Enable pcap tracing", tracing);

  //cmd.Parse (argc,argv);

  /*if (nWifi > 18)
    {
      std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box" << std::endl;
      return 1;
    }

  if (verbose)
    {
      LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
      LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }*/

  //Create csma 
  NodeContainer csmaNodes;
  csmaNodes.Create (nCsma);
 
  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
  csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
  NetDeviceContainer csmaDevices;
  csmaDevices = csma.Install (csmaNodes);
  std::cout<<"csma created\n";

  //create wifinode
  NodeContainer wifiStaNodes;
  wifiStaNodes.Create (nWifi);
  std::cout<<"wifi created\n";

  /*create channel
  NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default ();
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel= YansWifiChannelHelper::Default ();
  wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel", "MaxRange", DoubleValue(400));
  Ptr<YansWifiChannel> channel = wifiChannel.Create ();
  wifiPhy.SetChannel (channel); //创建通道对象并把他关联到物理层对象管理器
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);
  NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default ();
  Wifi80211pHelper wifi80211p = Wifi80211pHelper::Default ();
  wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                      "DataMode",StringValue ("OfdmRate6MbpsBW10MHz"),
                                      "ControlMode",StringValue ("OfdmRate6MbpsBW10MHz"));
  NetDeviceContainer wifidevices = wifi80211p.Install (wifiPhy, wifi80211pMac, csmaNodes);
  wifidevices.Add(wifi80211p.Install (wifiPhy, wifi80211pMac, wifiStaNodes));
  std::cout<<"device created\n";*/

NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default();
YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
YansWifiChannelHelper wifiChannel;
wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel","MaxRange", DoubleValue(190));
wifiPhy.SetChannel (wifiChannel.Create ());
Wifi80211pHelper wifi80211p = Wifi80211pHelper::Default();
wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"), "RtsCtsThreshold", UintegerValue (0),"ControlMode",StringValue("OfdmRate6MbpsBW10MHz"));
NetDeviceContainer wifidevices;
wifidevices = wifi80211p.Install (wifiPhy, wifi80211pMac, csmaNodes); //RSU
wifidevices.Add (wifi80211p.Install (wifiPhy, wifi80211pMac, wifiStaNodes)); //car 

  //range
   Ptr<UniformRandomVariable> x = CreateObject<UniformRandomVariable>();
   x->SetAttribute ("Min", DoubleValue (0));
   x->SetAttribute ("Max", DoubleValue (widthField));


   Ptr<UniformRandomVariable> y = CreateObject<UniformRandomVariable>();
   y->SetAttribute ("Min", DoubleValue (0));
   y->SetAttribute ("Max", DoubleValue (heightField));

  MobilityHelper mobility;
  //car
  mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
  mobility.Install(wifiStaNodes.Get(0));
  //Ptr<ConstantPositionMobilityModel> cpmm;
  Ptr<ConstantVelocityMobilityModel> cvmm;
  cvmm = wifiStaNodes.Get(0)->GetObject<ConstantVelocityMobilityModel> ();
  cvmm->SetPosition(Vector(-10,0,0));
  //cvmm->SetVelocity(Vector(12,0,0));
  std::cout<<"mb created\n";
  //rsu
//変更---------------------------------------------------
  for(uint16_t i=1;i<=nWifi-1;i++){
//-------------------------------------------------------
    mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
    mobility.Install(wifiStaNodes.Get(i));
    cvmm = wifiStaNodes.Get(i)->GetObject<ConstantVelocityMobilityModel> ();

    int x = rand()%2;
    int y = rand()%4;
    int z = rand()%4;
    int k = rand()%2;

    uint16_t a = rand()%1000+201; //横軸車両の初期位置
    uint16_t b = rand()%1000+1; //縦軸車両の初期位置

    if(x==0){
      switch(y){
        case 0:
          cvmm->SetPosition(Vector(a,0,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;
        case 1:
          cvmm->SetPosition(Vector(a,400,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;
        case 2:
          cvmm->SetPosition(Vector(a,800,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;
        case 3:
          cvmm->SetPosition(Vector(a,1200,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;
        /*case 4:
          cvmm->SetPosition(Vector(a,900,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;
        case 5:
          cvmm->SetPosition(Vector(a,1000,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;
        case 6:
          cvmm->SetPosition(Vector(a,1100,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;
        case 7:
          cvmm->SetPosition(Vector(a,1200,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;
        case 8:
          cvmm->SetPosition(Vector(a,1300,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;        
        case 9:
          cvmm->SetPosition(Vector(a,1400,0));
          if(k==0){
            cvmm->SetVelocity(Vector(12,0,0));
          }else{
            cvmm->SetVelocity(Vector(-12,0,0));
          }
          break;*/
        default:
          break;
      }
    }
    if(x==1){
      switch(z){
        case 0:
          cvmm->SetPosition(Vector(0,b,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        case 1:
          cvmm->SetPosition(Vector(400,b,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        case 2:
          cvmm->SetPosition(Vector(800,b,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        case 3:
          cvmm->SetPosition(Vector(1200,b,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        /*case 4:
          cvmm->SetPosition(Vector(600,b+500,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        case 5:
          cvmm->SetPosition(Vector(700,b+500,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        case 6:
          cvmm->SetPosition(Vector(800,b+500,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        case 7:
          cvmm->SetPosition(Vector(900,b+500,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        case 8:
          cvmm->SetPosition(Vector(1000,b+500,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;
        case 9:
          cvmm->SetPosition(Vector(1100,b+500,0));
          if(k==0){
            cvmm->SetVelocity(Vector(0,12,0));
          }else{
            cvmm->SetVelocity(Vector(0,-12,0));
          }
          break;*/          
        default:
          break;
      }
    }
  }
  /*for(uint8_t i=1;i<=10;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiStaNodes.Get(i));
    cpmm = wifiStaNodes.Get(i)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(rand()%1000,0,0));
  }
  for(uint8_t i=11;i<=20;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiStaNodes.Get(i));
    cpmm = wifiStaNodes.Get(i)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(400,rand()%1200,0));
  }
  for(uint8_t i=21;i<=29;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiStaNodes.Get(i));
    cpmm = wifiStaNodes.Get(i)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(rand()%1200,800,0));
  }*/
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (step),
                                 "DeltaY", DoubleValue (step),
                                 "GridWidth", UintegerValue (4),
                                 "LayoutType", StringValue ("RowFirst"));

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (csmaNodes);

  std::cout<<"mobi created\n";

  ttyHelper ttyProtocol;
  
  Ipv4ListRoutingHelper listrouting;
  listrouting.Add(ttyProtocol, 10);

  InternetStackHelper internet;
  internet.SetRoutingHelper(listrouting);
  internet.Install (csmaNodes);
  internet.Install (wifiStaNodes);
  std::cout<<"stack created\n";
  Ipv4AddressHelper ipv4;

  //  IP
  ipv4.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterfaces;
  csmaInterfaces = ipv4.Assign (csmaDevices);

  ipv4.SetBase ("10.1.3.0", "255.255.255.0");
  Ipv4InterfaceContainer wifiInterfaces;
  wifiInterfaces = ipv4.Assign (wifidevices);
  //ipv4.Assign (wifidevices);
  //csmaInterfaces = ipv4.Assign (wifidevices);
  std::cout<<"add created\n";

  AnimationInterface anim ("100size2.xml");
  Simulator::Stop(Seconds (20));
  //AsciiTraceHelper ascii;
  //pointToPoint.EnableAsciiAll(ascii.CreateFileStream("myfirst.tr");
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
